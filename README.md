﻿# Coparacion De Merge Sort con Insertion Sort #

### Descripcion ###
Este repo contiene la implementación en python de un programa que compara los tiempos de Insertion Sort con los de Merge Sort.
Usa arreglos generados aleatoriamente que crecen de 5 en 5 y escribe los tiempos de ejecucion de ambos algoritmos
en un archivo csv.

### Creado por: ###
Daniel Arroyo