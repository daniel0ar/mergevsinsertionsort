import funciones as f
import timeit as ti

archivo = open("log.csv", "w")
archivo.write("n,Merge Sort,Insertion Sort\n")
for i in range(1, 101):
    n = i * 5
    print("n= ", n)
    arreglo = f.crearArrayAleatorio(n)
    algoritmoMerge = f.wrapper(f.mergeSort, arreglo)
    tiempoMerge = ti.timeit(algoritmoMerge, number=1)
    print("\tTiempo total (MERGE): ","%.10f" % tiempoMerge)

    algoritmoInsertion = f.wrapper(f.insertionSort, arreglo)
    tiempoInsertion = ti.timeit(algoritmoInsertion, number=1)
    print("\tTiempo total (INSERTION): ", "%.10f" % tiempoInsertion)
    archivo.write(str(n)+","+str(tiempoMerge)+","+str(tiempoInsertion)+"\n")
    print()
