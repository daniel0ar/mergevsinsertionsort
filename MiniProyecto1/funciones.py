import random as rd


# FUNCION PARA CREAR ARREGLO ALEATORIO
def crearArrayAleatorio(tamano):
    arreglo = []
    for i in range(0, tamano):
        arreglo.append(rd.randint(0, tamano))
    return arreglo


# MERGE SORT
def merge(arrIzq, arrDer):
    nuevoArray = []
    while len(arrIzq) > 0 and len(arrDer) > 0:
        if arrIzq[0] <= arrDer[0]:
            nuevoArray.append(arrIzq[0])
            arrIzq.pop(0)
        else:
            nuevoArray.append(arrDer[0])
            arrDer.pop(0)
    if len(arrIzq) > 0:
        for elemento in arrIzq:
            nuevoArray.append(elemento)
    if len(arrDer) > 0:
        for elemento in arrDer:
            nuevoArray.append(elemento)
    return nuevoArray


def mergeSort(arreglo):
    izq = []
    der = []
    nuevoArray = []
    if len(arreglo) <= 1:
        return arreglo
    else:
        mitad = len(arreglo) // 2
        for i in range(mitad):
            izq.append(arreglo[i])
        for i in range(mitad, len(arreglo)):
            der.append(arreglo[i])
        izq = mergeSort(izq)
        der = mergeSort(der)
        if izq[-1] <= der[0]:
            for i in range(len(der)):
                izq.append(der[i])
            return izq
        nuevoArray = merge(izq, der)
        return nuevoArray


# INSERTION SORT
def insertionSort(arreglo):
    for i in range(1, len(arreglo)):
        llave = arreglo[i]
        j = i - 1
        while j >= 0 and llave < arreglo[j]:
            arreglo[j + 1] = arreglo[j]
            j -= 1
        arreglo[j + 1] = llave


# WRAPPER PARA TOMAR EL TIEMPO
def wrapper(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)
    return wrapped
